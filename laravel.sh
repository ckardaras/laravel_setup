#!/bin/sh


#Standard Update
sudo apt-get update
yes|sudo apt-get upgrade


#Install Atom
sudo apt install snapd
sudo snap install --classic atom  

#Install 2 Atom Packages
apm install language-blade
apm install atom-beautify

#Install Git
yes|sudo apt install git
git config --global credential.helper store

#install Laravel Environment

#install Laravel
yes|sudo apt-get install -y git curl wget zip unzip
yes|sudo apt-get install apache2
sudo service apache2 stop

#install Mysql Server
yes|sudo apt-get install mysql-server
echo "CREATE USER $USER@'localhost' IDENTIFIED BY '';GRANT ALL PRIVILEGES ON * . * TO $USER@'localhost';FLUSH PRIVILEGES;" | sudo mysql -u root
echo "Alter USER 'root'@'localhost' Identified with mysql_native_password by '';"| mysql

#install workbench
wget https://dev.mysql.com/get/Downloads/MySQLGUITools/mysql-workbench-community_8.0.20-1ubuntu20.04_amd64.deb
sudo dpkg -i mysql-workbench-community_8.0.20-1ubuntu20.04_amd64.deb

#install php and repositories
yes|sudo add-apt-repository ppa:ondrej/php
sudo apt-get update
yes|sudo apt-get install -y php7.1 php7.1-fpm libapache2-mod-php7.0 php7.1-cli php7.1-curl php7.1-mysql php7.1-sqlite3 php7.1-gd php7.1-xml php7.1-mcrypt php7.1-mbstring php7.1-iconv
yes|sudo apt-get install php7.1-zip

#install composer
curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer

#install laravel
composer global require laravel/installer
cd ~
echo 'export PATH=$PATH:$HOME/.config/composer/vendor/bin' >> .bashrc